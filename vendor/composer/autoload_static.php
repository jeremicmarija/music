<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitabc50a403ccdee3b46f9c720e9893c08
{
    public static $files = array (
        'd810d63de0323ba046b97a80b5572ba5' => __DIR__ . '/../..' . '/app/index.php',
    );

    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'Models\\' => 7,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Models\\' => 
        array (
            0 => __DIR__ . '/../..' . '/models',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitabc50a403ccdee3b46f9c720e9893c08::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitabc50a403ccdee3b46f9c720e9893c08::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
