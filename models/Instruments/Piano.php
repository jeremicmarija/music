<?php
/**
 * Created by PhpStorm.
 * User: smrda
 * Date: 9/13/17
 * Time: 9:12 PM
 */

namespace Models\Instruments;

use Models\Instrument;

class Piano extends Instrument
{

    public function __construct()
    {

        parent::__construct('piano');
    }
}