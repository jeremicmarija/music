<?php
/**
 * Created by PhpStorm.
 * User: smrda
 * Date: 9/13/17
 * Time: 9:06 PM
 */

namespace Models;

abstract class Instrument
{

    const VOLUME_MIN = 0;
    const VOLUME_MID = 1;
    const VOLUME_LOUD = 2;

    protected $sound;

    public function __construct($sound)
    {
        $this->sound = $sound;
    }

    public function play($volume = Instrument::VOLUME_MID)
    {

        switch ($volume) {
            case Instrument::VOLUME_MIN:
                return strtolower($this->sound);
            case Instrument::VOLUME_MID:
                return ucfirst(strtolower($this->sound));
            case Instrument::VOLUME_LOUD:
                return strtoupper($this->sound);
        }

        return '';
    }

}