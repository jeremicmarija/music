<?php
/**
 * Created by PhpStorm.
 * User: smrda
 * Date: 9/13/17
 * Time: 9:16 PM
 */
namespace Models;

class Dirigent
{

    const INSTRUMENT = 0;

    const VOLUME = 1;

    public $instruments = array();

    public $music = array();

    public function __construct(array $instruments)
    {

        foreach ($instruments as $instrument) {

            $this->addInstrument($instrument);
        }
    }

    public function addInstrument(Instrument $instrument)
    {

        $this->instruments[get_class($instrument)] = $instrument;
    }

    public function playMusic(array $notes)
    {

        foreach ($notes as $data) {

            if (empty($this->instruments[$data[Dirigent::INSTRUMENT]])) {

                continue;
            }

            echo $this->instruments[$data[Dirigent::INSTRUMENT]]->play($data[Dirigent::VOLUME]);
        }
    }
}